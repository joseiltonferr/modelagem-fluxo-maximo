# Modelagem Fluxo Máximo

Projeto de modelagem do problema do fluxo máximo para a disciplina de Pesquisa Operacional - UFPB. 

Utilizando-se da transformação de um *Problema do Fluxo Máximo* (PFM) em um *Problema de Fluxo de Custo Mínimo* (PFCM).

Realizando os ajustes necessários para a transformação e utilizando o solver *OR-Tools* para a resolução do PFCM.
