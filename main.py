# File name: main.py
# Author: Joseilton Ferreira
# Created: 25-out-2020
# Last Modified: 30-out-2020
# Python Version: 3.7.1

import sys
from ortools.graph import pywrapgraph

# extrai informacao unica sem comentario
def extractSingleData(string):
    out = ''
    for x in string:
        if x != (' ' or '\t'):
            out = out + x
        else:
            break
    return out


# extrai informacao multipla sem comentario
def extractMultData(string):
    aux = ''
    for x in string:
        if x != '#':
            aux = aux + x
        else:
            break
    out = aux.rstrip('\n\t ').split(' ')
    return out

with open(sys.argv[1], 'r') as file:

# extraindo informacao do arquivo de entrada
    vertex_number = int(extractSingleData(file.readline()))
    arc_number = int(extractSingleData(file.readline()))
    source = int(extractSingleData(file.readline())) - 1
    sink = int(extractSingleData(file.readline())) - 1

# array de inicio, final, capacidades, custos e demandas
    i_nodes = []
    j_nodes = []
    capacities = []
    costs = []
    supplies = []

# add informacao do arquivo de entrada para os arrays
    for x in range(arc_number):
        aux = extractMultData(file.readline())
        i_nodes.append(int(aux[0]) - 1)
        j_nodes.append(int(aux[1]) - 1)
        capacities.append(int(aux[2]))

# fluxo de oferta e de demanda
    flow_supply = 0
    flow_demmand = 0
    for x in range(arc_number):
        if i_nodes[x] == source:
            flow_supply += capacities[x]
        elif j_nodes[x] == sink:
            flow_demmand += capacities[x]

# add arco da origem para o escoadouro e sua capacidade
    i_nodes.append(source)
    j_nodes.append(sink)
    capacities.append(999999999999)

# fluxo viavel que sera utilizado como oferta e demanda
    security_flow = min(flow_supply, flow_demmand)
    for x in range(vertex_number):
        if x == source:
            supplies.append(security_flow)
        elif x == sink:
            supplies.append(-security_flow)
        else:
            supplies.append(0)

    arc_number += 1

# add custo zero para os arcos originais e custo max para o arco que sai da origem para o escoadouro
    for x in range(arc_number):
        if x == (arc_number - 1):
            costs.append(999999999999)
        else:
            costs.append(0)

# criando o problema de custo de fluxo minimo
prob = pywrapgraph.SimpleMinCostFlow()

# add arcos ao problema
for x in range(arc_number):
    prob.AddArcWithCapacityAndUnitCost(i_nodes[x], j_nodes[x], capacities[x], costs[x])

# add demanda/oferta de cada no
for x in range(vertex_number):
    prob.SetNodeSupply(x, supplies[x])

# chamando o solver do ortools e imprimindo na tela o resultado
if prob.Solve() == prob.OPTIMAL:
    max_flow = 0
    for x in range(prob.NumArcs()):
        if (prob.Tail(x) != source and prob.Head(x) == sink):
            max_flow += prob.Flow(x)
    print(f'\nMaximum Flow:  {max_flow}\n')
    print('    Arc      Flow / Capacity')
    for x in range(prob.NumArcs()):
        if not ((prob.Tail(x) == source and prob.Head(x) == sink) or prob.Flow(x) == 0):
            print(f'{prob.Tail(x) + 1:3} \u2500\u2500> {prob.Head(x) + 1:2}   {prob.Flow(x):4} / {prob.Capacity(x):4}')
else:
    print('Data input error')
